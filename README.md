# Google News & Natural Language Toolkit (NLTK)
This project was realized in python using several modules.

It allows to analyze the results given by google and to draw a global score.

Example: What google think about "Donald Trump" ?

## Installation:

Python3: https://www.python.org/

```bash
$ pip3 install textblob bs4 requests
```
