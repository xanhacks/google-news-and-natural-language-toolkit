#!/usr/bin/env python3
from textblob import TextBlob
from bs4 import BeautifulSoup
import requests

class Analysis:
	def __init__(self, search):
		self.search = search
		self.sentiment = 0
		self.note = 0
		self.precision = 1000
		self.url = f"https://www.google.com/search?q={self.search}&lr=lang_en&client=google-csbe&tbm=nws&cr=countryUS&num={self.precision}"

	def run(self):
		response = requests.get(self.url)
		soup = BeautifulSoup(response.text, 'html.parser')
		headline_results = soup.find_all('div', class_='st')
		for text in headline_results:
			blob = TextBlob(text.get_text())
			self.sentiment += blob.sentiment.polarity / len(headline_results)


	def display(self):
		self.note = round(self.sentiment * 5 + 5, 2)
		print("What does google have to say about it?")
		print(f"Word: {self.search}")
		print(f"Note: {self.note}/10")
		 
res = Analysis("Donald Trump")
res.run()
res.display()
